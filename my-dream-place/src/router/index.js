import { createRouter, createWebHistory } from "vue-router";

// Home
import HomePage from "@/pages/Home/HomePage.vue";

// Auth
import LoginPage from "@/pages/Auth/Login/Login.vue";
import RegisterPage from "@/pages/Auth/Register/Register.vue";

// Booking
import SearchPage from "@/pages/Booking/Search/Search.vue";
import OverviewPage from "@/pages/Booking/Overview/Overview.vue";
import PaymentPage from "@/pages/Booking/Payment/Payment.vue";
import MyTripsPage from "@/pages/Booking/MyTripsPage/MyTrips.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomePage,
    },
    {
      path: "/auth",
      name: "auth",
      redirect: { name: "login" },
      children: [
        {
          path: "login",
          name: "login",
          component: LoginPage,
        },
        {
          path: "register",
          name: "register",
          component: RegisterPage,
        },
      ],
      // Guard
      beforeEnter: () => {
        const isLoggedIn = window.localStorage.getItem("accessToken");

        if (isLoggedIn) {
          return { name: "home", replace: true };
        }
      },
    },
    {
      path: "/booking",
      name: "postLogin",
      redirect: { name: "search" },
      children: [
        {
          path: "search",
          name: "search",
          component: SearchPage,
        },
        {
          path: "overview/:arrivalDate/:departureDate/:id",
          name: "overview",
          component: OverviewPage,
          props: true,
        },
        {
          path: "payment",
          name: "payment",
          component: PaymentPage,
        },
        {
          path: "mytrips",
          name: "mytrips",
          component: MyTripsPage,
        },
      ],
      beforeEnter: () => {
        const isLoggedIn = window.localStorage.getItem("accessToken");

        if (!isLoggedIn) {
          return { name: "login", replace: true };
        }
      },
    },
  ],
});

export default router;
