import { ref } from "vue";
import { defineStore } from "pinia";
import axios from "axios";

export const useSearchResultStore = defineStore("searchResult", () => {
  const loading = ref(false);
  const searchResult = ref([]);
  const sortByOptions = ref([]);
  const searchQuery = ref({
    destination: "",
    arrivalDate: "",
    departureDate: "",
    numOfAdults: "",
    numOfRooms: "",
  });

  const saveSearchQuery = (
    destination,
    arrivalDate,
    departureDate,
    numOfAdults,
    numOfRooms
  ) => {
    searchQuery.value = {
      destination,
      arrivalDate,
      departureDate,
      numOfAdults,
      numOfRooms,
    };
  };

  const getSearchResults = async (
    minPrice = undefined,
    maxPrice = undefined,
    sortById = undefined
  ) => {
    loading.value = true;

    const { destination, arrivalDate, departureDate, numOfAdults, numOfRooms } =
      searchQuery.value;

    const options = {
      method: "GET",
      url: "https://booking-com15.p.rapidapi.com/api/v1/hotels/searchHotels",
      params: {
        dest_id: destination,
        search_type: "CITY",
        arrival_date: arrivalDate,
        departure_date: departureDate,
        adults: numOfAdults,
        children_age: "0,17",
        room_qty: numOfRooms,
        page_number: "1",
        languagecode: "en-us",
        currency_code: "USD",
        price_min: minPrice,
        price_max: maxPrice,
        sort_by: sortById,
      },
      headers: {
        "X-RapidAPI-Key": "f9730c6451mshb6ad0c8fa6c0871p1c7371jsn3d477cf4ed97",
        "X-RapidAPI-Host": "booking-com15.p.rapidapi.com",
      },
    };

    try {
      const getHotelsResponse = await axios.request(options);

      searchResult.value = getHotelsResponse.data?.data?.hotels;

      const sortOptionsResponse = await axios.request({
        ...options,
        url: "https://booking-com15.p.rapidapi.com/api/v1/hotels/getSortBy",
      });

      sortByOptions.value = sortOptionsResponse.data?.data;
    } catch (error) {
      console.error(error);
    } finally {
      loading.value = false;
    }
  };

  return {
    loading,
    searchResult,
    searchQuery,
    getSearchResults,
    saveSearchQuery,
    sortByOptions,
  };
});
