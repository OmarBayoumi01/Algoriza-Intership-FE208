export const destinationOptions = [
  {
    name: "Cairo",
    value: "-290692",
  },
  {
    name: "Aswan",
    value: "-291535",
  },
  {
    name: "Luxor",
    value: "-290821",
  },
];

export const getDestinationName = (value) => {
  const foundOption = destinationOptions.find(
    (option) => option.value === value
  );

  if (foundOption) {
    return foundOption.name;
  }

  return null;
};
