import { ref } from "vue";
import { defineStore } from "pinia";
import axios from "axios";

export const useHotelInfo = defineStore("useHotelInfo", () => {
  const loading = ref(false);
  const hotelInfo = ref({});

  const getHotelInfo = async (
    hotelId = undefined,
    arrivalDate = undefined,
    departureDate = undefined
  ) => {
    loading.value = true;

    const options = {
      method: "GET",
      url: "https://booking-com15.p.rapidapi.com/api/v1/hotels/getHotelDetails",
      params: {
        hotel_id: hotelId,
        arrival_date: arrivalDate,
        departure_date: departureDate,
        currency_code: "USD",
      },
      headers: {
        "X-RapidAPI-Key": "f9730c6451mshb6ad0c8fa6c0871p1c7371jsn3d477cf4ed97",
        "X-RapidAPI-Host": "booking-com15.p.rapidapi.com",
      },
    };

    try {
      const response = await axios.request(options);

      hotelInfo.value = response.data?.data;
    } catch (error) {
      console.error(error);
    } finally {
      loading.value = false;
    }
  };

  return {
    loading,
    hotelInfo,
    getHotelInfo,
  };
});
